package trabalho2_pss.Presenter;

import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.JOptionPane;
import org.springframework.web.client.ResourceAccessException;
import trabalho2_pss.Service.MenuPrincipalService;
import trabalho2_pss.View.MenuPrincipalView;

public class MenuPrincipalPresenter {

    private static MenuPrincipalPresenter instancia;

    private MenuPrincipalState state;

    private MenuPrincipalView view;

    private MenuPrincipalPresenter() {
        initView();
        this.view = MenuPrincipalView.getInstancia();
        this.state = new FuncionandoState(this);
    }

    public static MenuPrincipalPresenter getInstancia() {
        if (instancia == null) {
            instancia = new MenuPrincipalPresenter();
        }
        return instancia;
    }

    private void initView() {
        view = MenuPrincipalView.getInstancia();
        atualizarTabelaCedulasDisponiveis();

        view.adicionarActionListenerButtonBuscaData((ActionEvent e) -> {
            opcaoBuscarDataPressionado();
        });

        view.adicionarActionListenerButtonBuscaId((ActionEvent e) -> {
            opcaoBuscarIDPressionado();
        });

        view.adicionarActionListenerButtonInserir((ActionEvent e) -> {
            opcaoInserirPressionado();
        });

        view.adicionarActionListenerButtonManutencao((ActionEvent e) -> {
            opcaoManutencaoPressionado();
        });

        view.adicionarActionListenerButtonTodasOperacoes((ActionEvent e) -> {
            opcaoTodasOperacoesPressionado();
        });

        view.adicionarActionListenerButtonSacar((ActionEvent e) -> {
            opcaoSacarPressionado();
        });

        view.adicionarActionListenerButtonUltimaOp((ActionEvent e) -> {
            opcaoUltimaOpPressionado();
        });
    }

    public void setViewVisible(boolean b) {
        view.setVisible(b);
    }

    public void setHabilitaOperacoes(boolean b) {
        view.setHabilitaOperacoes(b);
    }

    public void atualizarTabelaCedulasDisponiveis() {
        var lista = MenuPrincipalService.getConteudo();
        view.limparTabelaCedulasDisponiveis();
        view.preencheTabela(lista);
    }
    
    public void setState(MenuPrincipalState state) {
        this.state = state;
    }
    
    // -------------------------------------------------------------------------
    // Ponte com a view
    // -------------------------------------------------------------------------
    public String getValorCampoBuscaPorData() {
        return view.getValorCampoBuscaPorData();
    }
    
    public void limparTabelaOperacoes() {
        view.limparTabelaOperacoes();
    }
    
    public void preencherTabelaOperacoes(String[] operacoes) {
        view.preencherTabelaOperacoes(operacoes);
    }
    
    public String getValorCampoBuscaPorId() {
        return view.getValorCampoBuscaPorId();
    }
    
    public void terminarEdicaoTabelaInserirCedulas() {
        view.terminarEdicaoTabelaInserirCedulas();
    }
    
    public Map<String, String> getValoresTabelaInsereCedulas() {
        return view.getValoresTabelaInsereCedulas();
    }
    
    public void limparTabelaInserirValores() {
        view.limparTabelaInserirValores();
    }
    
    public String getValorCampoSaque() {
        return view.getValorCampoSaque();
    }

    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void opcaoBuscarDataPressionado() {
        try {
            state.opcaoBuscarDataPressionado();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void opcaoBuscarIDPressionado() {
        try {
            state.opcaoBuscarIDPressionado();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void opcaoInserirPressionado() {
        try {
            state.opcaoInserirPressionado();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void opcaoManutencaoPressionado() {
        state.opcaoManutencaoPressionado();
    }

    private void opcaoTodasOperacoesPressionado() {
        try {
            state.opcaoTodasOperacoesPressionado();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void opcaoSacarPressionado() {
        try {
            state.opcaoSacarPressionado();
        } catch (ResourceAccessException ex) {
            JOptionPane.showMessageDialog(view, "Timeout", "Erro", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void opcaoUltimaOpPressionado() {
        try {
            state.opcaoUltimaOpPressionado();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.toString(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

}
