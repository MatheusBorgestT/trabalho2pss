package trabalho2_pss.Presenter;

import javax.swing.JOptionPane;
import trabalho2_pss.Service.MenuPrincipalService;

public class ManutencaoState extends MenuPrincipalState {

    public ManutencaoState(MenuPrincipalPresenter presenter) {
        super(presenter);
    }

    @Override
    public void configurarTela() {
        presenter.setHabilitaOperacoes(false);
    }

    @Override
    public void opcaoBuscarDataPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoBuscarIDPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoInserirPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoManutencaoPressionado() {
        int resposta = JOptionPane.showConfirmDialog(null, "O sistema sairá do modo de manutenção. Deseja continuar?");
        if (resposta == 0) {
            presenter.setState(new FuncionandoState(presenter));
            MenuPrincipalService.manutencao(false);
            presenter.atualizarTabelaCedulasDisponiveis();
        }
    }

    @Override
    public void opcaoTodasOperacoesPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoSacarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoUltimaOpPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
