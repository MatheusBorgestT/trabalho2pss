
package trabalho2_pss.Presenter;

public abstract class MenuPrincipalState {
    
    protected MenuPrincipalPresenter presenter;
    
    public MenuPrincipalState(MenuPrincipalPresenter presenter) {
        this.presenter = presenter;
        configurarTela();
    }
    
    public abstract void configurarTela();
    
    public abstract void opcaoBuscarDataPressionado();

    public abstract void opcaoBuscarIDPressionado();

    public abstract void opcaoInserirPressionado();

    public abstract void opcaoManutencaoPressionado();

    public abstract void opcaoTodasOperacoesPressionado();

    public abstract void opcaoSacarPressionado();

    public abstract void opcaoUltimaOpPressionado();
}
