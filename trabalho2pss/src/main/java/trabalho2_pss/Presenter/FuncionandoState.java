package trabalho2_pss.Presenter;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JOptionPane;
import trabalho2_pss.Model.SaqueForm;
import trabalho2_pss.Model.PapelMoeda;
import trabalho2_pss.Model.ValorForm;
import trabalho2_pss.Service.MenuPrincipalService;
import trabalho2_pss.Util.BuscaPorDataValidador;
import trabalho2_pss.Util.BuscaPorIdValidador;
import trabalho2_pss.Util.InserirValidador;
import trabalho2_pss.Util.SaqueValidador;

public class FuncionandoState extends MenuPrincipalState {

    public FuncionandoState(MenuPrincipalPresenter presenter) {
        super(presenter);
    }

    @Override
    public void configurarTela() {
        presenter.setHabilitaOperacoes(true);
    }

    @Override
    public void opcaoBuscarDataPressionado() {
        String strValor = presenter.getValorCampoBuscaPorData();
        String validacao = new BuscaPorDataValidador().validar(strValor);

        if (validacao.equals("OK")) {
            var dtf1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            var dtf2 = DateTimeFormatter.ofPattern("dd-MM-yyyy");

            var data = dtf2.format(dtf1.parse(strValor));

            String operacao = MenuPrincipalService.getOperacaoByData(data);
            presenter.limparTabelaOperacoes();
            presenter.preencherTabelaOperacoes(new String[]{operacao});
        } else {
            JOptionPane.showMessageDialog(null, validacao, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void opcaoBuscarIDPressionado() {
        String strValor = presenter.getValorCampoBuscaPorId();
        String validacao = new BuscaPorIdValidador().validar(strValor);

        if (validacao.equals("OK")) {
            String operacao = MenuPrincipalService.getOperacaoById(strValor);
            presenter.limparTabelaOperacoes();
            presenter.preencherTabelaOperacoes(new String[]{operacao});
        } else {
            JOptionPane.showMessageDialog(null, validacao, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void opcaoInserirPressionado() {
        presenter.terminarEdicaoTabelaInserirCedulas();

        var valores = presenter.getValoresTabelaInsereCedulas();
        ArrayList<PapelMoeda> listaPM = new ArrayList<>();

        String validacao, valor;
        int i = 1;
        for (Map.Entry<String, String> entry : valores.entrySet()) {
            valor = entry.getValue();
            if (valor != null && !valor.isBlank()) {
                validacao = new InserirValidador().validar(entry.getValue());
                if (!validacao.equals("OK")) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Erro na linha " + String.valueOf(i) + "\n" + validacao,
                            "Erro",
                            JOptionPane.ERROR_MESSAGE
                    );
                    return;
                }

                listaPM.add(new PapelMoeda(
                        Integer.parseInt(entry.getValue()),
                        Double.parseDouble(entry.getKey())
                ));
            }

            i++;
        }
        
        PapelMoeda[] array = new PapelMoeda[listaPM.size()];
        for (i = 0; i < listaPM.size(); i++) {
            array[i] = listaPM.get(i);
        }

        var form = new ValorForm(array);
        MenuPrincipalService.insereValores(form);
        
        presenter.limparTabelaInserirValores();
        presenter.atualizarTabelaCedulasDisponiveis();
        JOptionPane.showMessageDialog(
                null,
                "Valores inseridos com sucesso",
                "Sucesso",
                JOptionPane.INFORMATION_MESSAGE
        );
    }

    @Override
    public void opcaoManutencaoPressionado() {
        int resposta = JOptionPane.showConfirmDialog(null, "O sistema entrará em modo de manutenção. Deseja continuar?");
        if (resposta == 0) {
            presenter.setState(new ManutencaoState(presenter));
            MenuPrincipalService.manutencao(true);
        }
    }

    @Override
    public void opcaoTodasOperacoesPressionado() {
        String[] todasOperacoes = MenuPrincipalService.getTodasOperacoes();

        // Invertendo a ordem para ficar da mais atual para o mais antigo
        String temp;
        for (int i = 0; i < todasOperacoes.length / 2; i++) {
            temp = todasOperacoes[i];
            todasOperacoes[i] = todasOperacoes[todasOperacoes.length - 1 - i];
            todasOperacoes[todasOperacoes.length - 1 - i] = temp;
        }

        presenter.limparTabelaOperacoes();
        presenter.preencherTabelaOperacoes(todasOperacoes);
    }

    @Override
    public void opcaoSacarPressionado() {
        String strValor = presenter.getValorCampoSaque();
        String validacao = new SaqueValidador().validar(strValor);

        if (validacao.equals("OK")) {
            var form = new SaqueForm(Double.parseDouble(strValor));
            var saque = MenuPrincipalService.realizarSaque(form);
            JOptionPane.showMessageDialog(null, saque.getMensagem(), "Informação", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, validacao, "Erro", JOptionPane.ERROR_MESSAGE);
        }
        
        presenter.atualizarTabelaCedulasDisponiveis();
    }

    @Override
    public void opcaoUltimaOpPressionado() {
        String operacao = MenuPrincipalService.getUltimaOperacao();
        presenter.limparTabelaOperacoes();
        presenter.preencherTabelaOperacoes(new String[]{operacao});
    }

}
