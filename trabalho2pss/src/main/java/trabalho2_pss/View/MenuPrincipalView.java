package trabalho2_pss.View;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import trabalho2_pss.Model.PapelMoeda;

public class MenuPrincipalView extends javax.swing.JFrame {

    private static MenuPrincipalView instancia;

    private MenuPrincipalView() {
        initComponents();
    }

    public static MenuPrincipalView getInstancia() {
        if (instancia == null) {
            instancia = new MenuPrincipalView();
        }
        return instancia;
    }

    public void setHabilitaOperacoes(boolean b) {
        jButtonBuscaData.setEnabled(b);
        jButtonBuscaId.setEnabled(b);
        jButtonInserir.setEnabled(b);
        jButtonTodasOperacoes.setEnabled(b);
        jButtonSacar.setEnabled(b);
        jButtonUltimaOp.setEnabled(b);
        jFormattedTextFieldData.setEnabled(b);
        jTextFieldBuscaPorId.setEnabled(b);
        jTableCedulasDisponiveis.setEnabled(b);
        jTableInserirValores.setEnabled(b);
        jTableOperacoes.setEnabled(b);
        jTextFieldSacar.setEnabled(b);

        if (b) {
            jButtonManutencao.setText("Manutenção");
        } else {
            jButtonManutencao.setText("Sair da Manutenção");
            limparTabelaOperacoes();
            limparTabelaInserirValores();
            limparTabelaCedulasDisponiveis();
        }
    }

    public void limparTabelaCedulasDisponiveis() {
        var model = (DefaultTableModel) jTableCedulasDisponiveis.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            jTableCedulasDisponiveis.setValueAt("", i, 1);
        }
    }

    public void limparTabelaInserirValores() {
        var model = (DefaultTableModel) jTableInserirValores.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            jTableInserirValores.setValueAt("", i, 1);
        }
    }

    public void limparTabelaOperacoes() {
        var model = (DefaultTableModel) jTableOperacoes.getModel();
        model.setRowCount(0);
    }

    public void preencherTabelaOperacoes(String operacoes[]) {
        var model = (DefaultTableModel) jTableOperacoes.getModel();
        for (String o : operacoes) {
            model.addRow(new Object[]{o});
        }
    }

    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    public void adicionarActionListenerButtonBuscaData(ActionListener e) {
        jButtonBuscaData.addActionListener(e);
    }

    public void adicionarActionListenerButtonBuscaId(ActionListener e) {
        jButtonBuscaId.addActionListener(e);
    }

    public void adicionarActionListenerButtonInserir(ActionListener e) {
        jButtonInserir.addActionListener(e);
    }

    public void adicionarActionListenerButtonManutencao(ActionListener e) {
        jButtonManutencao.addActionListener(e);
    }

    public void adicionarActionListenerButtonTodasOperacoes(ActionListener e) {
        jButtonTodasOperacoes.addActionListener(e);
    }

    public void adicionarActionListenerButtonSacar(ActionListener e) {
        jButtonSacar.addActionListener(e);
    }

    public void adicionarActionListenerButtonUltimaOp(ActionListener e) {
        jButtonUltimaOp.addActionListener(e);
    }

    public Map<String, String> getValoresTabelaInsereCedulas() {
        Map<String, String> valores = new HashMap<>();
        for (int i = 0; i < jTableInserirValores.getRowCount(); i++) {
            valores.put(
                    (String) jTableInserirValores.getValueAt(i, 0),
                    (String) jTableInserirValores.getValueAt(i, 1)
            );
        }
        return valores;
    }

    public void terminarEdicaoTabelaInserirCedulas() {
        if (jTableInserirValores.isEditing()) {
            jTableInserirValores.getCellEditor().stopCellEditing();
        }
    }

    // Preenchendo as tabelas de cedula
    private double pegaCedula(int linha) {
        return Double.parseDouble(String.valueOf(jTableInserirValores.getValueAt(linha, 0)));
    }

    private boolean comparaValor(double valor, int linha) {
        return pegaCedula(linha) == valor;
    }

    public void preencheTabela(PapelMoeda lista[]) {
        int rows = jTableCedulasDisponiveis.getRowCount();
        for (PapelMoeda pm : lista) {
            for (int i = 0; i < rows; i++) {
                if (comparaValor(pm.getValorCedulaMoeda(), i)) {
                    jTableCedulasDisponiveis.setValueAt(pm.getQuantidade(), i, 1);
                    break;
                }
            }
        }
    }

    public void zeraCampos() {
        int i, j = jTableInserirValores.getRowCount();
        for (i = 1; i <= j; i++) {
            jTableInserirValores.setValueAt("", i, 1);
        }
    }

    // -------------------------------------------------------------------------
    // Getters
    // -------------------------------------------------------------------------
    public String getValorCampoSaque() {
        return jTextFieldSacar.getText();
    }

    public String getValorCampoBuscaPorId() {
        return jTextFieldBuscaPorId.getText();
    }

    public String getValorCampoBuscaPorData() {
        return jFormattedTextFieldData.getText();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonSacar = new javax.swing.JButton();
        jTextFieldSacar = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableCedulasDisponiveis = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableInserirValores = new javax.swing.JTable();
        jButtonInserir = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableOperacoes = new javax.swing.JTable();
        jButtonTodasOperacoes = new javax.swing.JButton();
        jButtonUltimaOp = new javax.swing.JButton();
        jButtonBuscaId = new javax.swing.JButton();
        jFormattedTextFieldData = new javax.swing.JFormattedTextField();
        jButtonBuscaData = new javax.swing.JButton();
        jTextFieldBuscaPorId = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jButtonManutencao = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setEnabled(false);

        jButtonSacar.setText("Sacar");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextFieldSacar)
                .addGap(18, 18, 18)
                .addComponent(jButtonSacar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSacar)
                    .addComponent(jTextFieldSacar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTableCedulasDisponiveis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"200", null},
                {"100", null},
                {"50", null},
                {"20", null},
                {"10", null},
                {"5", null},
                {"2", null},
                {"1", null},
                {"0.50", null},
                {"0.25", null},
                {"0.10", null},
                {"0.05", null},
                {"0.01", null}
            },
            new String [] {
                "Cédula (R$)", "Quantidade"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTableCedulasDisponiveis);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTableInserirValores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"200", null},
                {"100", null},
                {"50", null},
                {"20", null},
                {"10", null},
                {"5", null},
                {"2", null},
                {"1", null},
                {"0.50", null},
                {"0.25", null},
                {"0.10", null},
                {"0.05", null},
                {"0.01", null}
            },
            new String [] {
                "Cédula (R$)", "Quantidade"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableInserirValores);

        jButtonInserir.setText("Inserir Valores");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonInserir)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonInserir)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTableOperacoes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Operação"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableOperacoes);
        if (jTableOperacoes.getColumnModel().getColumnCount() > 0) {
            jTableOperacoes.getColumnModel().getColumn(0).setResizable(false);
        }

        jButtonTodasOperacoes.setText("Todas as Operações");

        jButtonUltimaOp.setText("Última Operação");

        jButtonBuscaId.setText("Busca Por ID");

        try {
            jFormattedTextFieldData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextFieldData.setText("");

        jButtonBuscaData.setText("Busca Por Data");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 747, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonTodasOperacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonUltimaOp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jFormattedTextFieldData, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(jTextFieldBuscaPorId))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonBuscaData, javax.swing.GroupLayout.PREFERRED_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(jButtonBuscaId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonBuscaId)
                    .addComponent(jButtonTodasOperacoes)
                    .addComponent(jTextFieldBuscaPorId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonBuscaData)
                    .addComponent(jFormattedTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonUltimaOp))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 631, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonManutencao.setText("Manutenção");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonManutencao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonManutencao)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBuscaData;
    private javax.swing.JButton jButtonBuscaId;
    private javax.swing.JButton jButtonInserir;
    private javax.swing.JButton jButtonManutencao;
    private javax.swing.JButton jButtonSacar;
    private javax.swing.JButton jButtonTodasOperacoes;
    private javax.swing.JButton jButtonUltimaOp;
    private javax.swing.JFormattedTextField jFormattedTextFieldData;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTableCedulasDisponiveis;
    private javax.swing.JTable jTableInserirValores;
    private javax.swing.JTable jTableOperacoes;
    private javax.swing.JTextField jTextFieldBuscaPorId;
    private javax.swing.JTextField jTextFieldSacar;
    // End of variables declaration//GEN-END:variables
}
