package trabalho2_pss.Model;

import java.util.Date;

public class Saque {

    private Date dataHora;
    private String mensagem;
    private double valorDoSaque;

    public Saque() {
    }

    public Saque(Date dataHora, String mensagem, double valorDoSaque) {
        this.dataHora = dataHora;
        this.mensagem = mensagem;
        this.valorDoSaque = valorDoSaque;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public double getValorDoSaque() {
        return valorDoSaque;
    }

    public void setValorDoSaque(double valorDoSaque) {
        this.valorDoSaque = valorDoSaque;
    }

}
