
package trabalho2_pss.Model;

public class SaqueForm {
    
    private double valorDoSaque;

    public SaqueForm() {
    }

    public SaqueForm(double valorDoSaque) {
        this.valorDoSaque = valorDoSaque;
    }

    public double getValorDoSaque() {
        return valorDoSaque;
    }
    
}
