
package trabalho2_pss.Model;

public class ValorForm {
    
    private PapelMoeda[] valores;

    public ValorForm() {
    }

    public ValorForm(PapelMoeda[] valores) {
        this.valores = valores;
    }

    public PapelMoeda[] getValores() {
        return valores;
    }
    
}
