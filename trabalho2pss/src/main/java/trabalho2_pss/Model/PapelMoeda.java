package trabalho2_pss.Model;

public class PapelMoeda {

    private int quantidade;

    private double valorCedulaMoeda;

    public PapelMoeda() {

    }

    public PapelMoeda(int quantidade, double valorCedulaMoeda) {
        this.quantidade = quantidade;
        this.valorCedulaMoeda = valorCedulaMoeda;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public double getValorCedulaMoeda() {
        return valorCedulaMoeda;
    }

    @Override
    public String toString() {
        return ("Qtd: " + String.valueOf(quantidade) + "\tValorCedula: " + String.valueOf(valorCedulaMoeda));
    }

}
