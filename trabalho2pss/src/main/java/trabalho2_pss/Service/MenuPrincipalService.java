package trabalho2_pss.Service;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import trabalho2_pss.Model.PapelMoeda;
import trabalho2_pss.Model.Saque;
import trabalho2_pss.Model.SaqueForm;
import trabalho2_pss.Model.ValorForm;

public class MenuPrincipalService {

    private static final String ipPorta = "172.17.0.2:8080";
    private static final String baseUrl = "http://" + ipPorta + "/api/caixa_eletronico/v1/";
    private static final int timeout = 5000;

    public static PapelMoeda[] getConteudo() {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(baseUrl + "conteudo", PapelMoeda[].class);
        return response.getBody();
    }
    
    public static ValorForm insereValores(ValorForm form) {
        var restTemplate = new RestTemplate();
        var response = restTemplate.postForEntity(baseUrl + "insereValores", form, ValorForm.class);
        return response.getBody();
    }

    public static Saque realizarSaque(SaqueForm form) {
        var restTemplate = new RestTemplate();
        setTimeout(restTemplate);
        var response = restTemplate.postForEntity(baseUrl + "saque", form, Saque.class);
        return response.getBody();
    }

    public static String[] getTodasOperacoes() {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(baseUrl + "operacoes", String[].class);
        return response.getBody();
    }

    public static String getUltimaOperacao() {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(baseUrl + "ultimaOperacao", String.class);
        return response.getBody();
    }

    public static String getOperacaoById(String id) {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(baseUrl + "operacao/" + id, String.class);
        return response.getBody();
    }

    public static String getOperacaoByData(String data) {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(baseUrl + "operacoes/" + data, String.class);
        return response.getBody();
    }

    public static boolean manutencao(boolean b) {
        var restTemplate = new RestTemplate();
        var response = restTemplate.postForEntity(baseUrl + "manutencao", b, boolean.class);
        return response.getBody();
    }

    private static void setTimeout(RestTemplate restTemplate) {
        restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
        SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate
                .getRequestFactory();
        rf.setReadTimeout(timeout);
        rf.setConnectTimeout(timeout);
    }

}
