package trabalho2_pss.Util;

import trabalho2_pss.Util.Validadores.IValidador;
import trabalho2_pss.Util.Validadores.IntegerValidador;
import trabalho2_pss.Util.Validadores.ValorMaiorQueZeroValidador;
import trabalho2_pss.Util.Validadores.ValorVazioValidador;

public class InserirValidador {
    
    private final IValidador primeiro;

    public InserirValidador() {
        primeiro = new IntegerValidador();
        var segundo = new ValorMaiorQueZeroValidador();
        
        primeiro.setProximo(segundo);
    }
    
    public String validar(String quantidade) {
        return primeiro.validar(quantidade);
    }
}
