
package trabalho2_pss.Util;

import trabalho2_pss.Util.Validadores.DataValidador;
import trabalho2_pss.Util.Validadores.IValidador;
import trabalho2_pss.Util.Validadores.ValorVazioValidador;

public class BuscaPorDataValidador {
    
    private IValidador primeiro;
    
    public BuscaPorDataValidador() {
        primeiro = new ValorVazioValidador();
        var segundo = new DataValidador();
        
        primeiro.setProximo(segundo);
    }
    
    public String validar(String valor) {
        return primeiro.validar(valor);
    }
}
