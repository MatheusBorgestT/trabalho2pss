
package trabalho2_pss.Util.Validadores;

public class IntegerValidador extends AbstractValidador {

    @Override
    public String validar(String valor) {
        try {
            Integer.parseInt(valor);
            if (proximo == null) {
                return "OK";
            } else {
                return proximo.validar(valor);
            }
        } catch (NumberFormatException ex) {
            return "O valor informado não é inteiro";
        }
    }
    
}
