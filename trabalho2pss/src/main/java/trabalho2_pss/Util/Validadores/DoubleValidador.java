
package trabalho2_pss.Util.Validadores;

public class DoubleValidador extends AbstractValidador {

    @Override
    public String validar(String valor) {
        try {
            Double.parseDouble(valor);
            if (proximo == null) {
                return "OK";
            } else {
                return proximo.validar(valor);
            }
        } catch (NumberFormatException ex) {
            return "O valor informado não é real";
        }
    }
    
}
