
package trabalho2_pss.Util.Validadores;

public abstract class AbstractValidador implements IValidador {
    
    protected IValidador proximo;
    
    public void setProximo(IValidador validador) {
        proximo = validador;
    }
    
    public abstract String validar(String valor);
    
}
