
package trabalho2_pss.Util.Validadores;

public interface IValidador {
    
    public void setProximo(IValidador validador);
    
    public String validar(String valor);
    
}
