
package trabalho2_pss.Util.Validadores;

public class ValorMaiorQueZeroValidador extends AbstractValidador {

    @Override
    public String validar(String valor) {
        double dValor = Double.parseDouble(valor);
        if (dValor >= 0) {
            if (proximo == null) {
                return "OK";
            } else {
                return proximo.validar(valor);
            }
        } else {
            return "O valor precisa ser maior ou igual a zero";
        }
    }
    
}
