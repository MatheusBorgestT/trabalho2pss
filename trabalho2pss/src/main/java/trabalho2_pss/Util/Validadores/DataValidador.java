package trabalho2_pss.Util.Validadores;

import java.time.format.DateTimeFormatter;

public class DataValidador extends AbstractValidador {

    @Override
    public String validar(String valor) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dtf.parse(valor);
            if (proximo == null) {
                return "OK";
            } else {
                return proximo.validar(valor);
            }
        } catch (NumberFormatException ex) {
            return "O valor informado não é real";
        }
    }
    
}
