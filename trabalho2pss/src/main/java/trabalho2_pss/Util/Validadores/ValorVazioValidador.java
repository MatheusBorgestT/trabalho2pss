
package trabalho2_pss.Util.Validadores;

public class ValorVazioValidador extends AbstractValidador {

    @Override
    public String validar(String valor) {
        if (!valor.isBlank()) {
            if (proximo == null) {
                return "OK";
            } else {
                return proximo.validar(valor);
            }
        } else {
            return "O valor não pode ser vazio";
        }
    }
    
}
