
package trabalho2_pss.Util;

import trabalho2_pss.Util.Validadores.IValidador;
import trabalho2_pss.Util.Validadores.IntegerValidador;
import trabalho2_pss.Util.Validadores.ValorMaiorQueZeroValidador;
import trabalho2_pss.Util.Validadores.ValorVazioValidador;

public class BuscaPorIdValidador {
    
    private IValidador primeiro;
    
    public BuscaPorIdValidador() {
        primeiro = new ValorVazioValidador();
        var segundo = new IntegerValidador();
        var terceiro = new ValorMaiorQueZeroValidador();
        
        segundo.setProximo(terceiro);
        primeiro.setProximo(segundo);
    }
    
    public String validar(String valor) {
        return primeiro.validar(valor);
    }
    
}
