
package trabalho2_pss.Util;

import trabalho2_pss.Util.Validadores.DoubleValidador;
import trabalho2_pss.Util.Validadores.ValorMaiorQueZeroValidador;
import trabalho2_pss.Util.Validadores.IValidador;
import trabalho2_pss.Util.Validadores.ValorVazioValidador;

public class SaqueValidador {
    
    private IValidador primeiro;
    
    public SaqueValidador() {
        primeiro = new ValorVazioValidador();
        var segundo = new DoubleValidador();
        var terceiro = new ValorMaiorQueZeroValidador();
        
        segundo.setProximo(terceiro);
        primeiro.setProximo(segundo);
    }
    
    public String validar(String valor) {
        return primeiro.validar(valor);
    }
    
}
